package com.app.webui.service;

import com.app.webui.model.Forecast;
import com.app.webui.model.Measure;

public interface RestForecastService {

    Measure getCurrentMeasure();

    Forecast getForecast();
}
