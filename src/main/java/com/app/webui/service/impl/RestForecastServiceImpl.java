package com.app.webui.service.impl;

import com.app.webui.model.Forecast;
import com.app.webui.model.Measure;
import com.app.webui.service.RestForecastService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class RestForecastServiceImpl implements RestForecastService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("restTemplate")
    private OAuth2RestOperations restTemplate;

    private @Value("${config.oauth2.currentURI}") String urlCurrentMeasure;
    private @Value("${config.oauth2.forecastURI}") String urlForecast;

    @Override
    public Measure getCurrentMeasure() {

        String currentMeasure = restTemplate.getForObject(urlCurrentMeasure, String.class);

        ObjectMapper mapper = new ObjectMapper();
        JavaTimeModule module = new JavaTimeModule();
        mapper.registerModule(module);

        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        try {
            return mapper.readValue(currentMeasure, Measure.class);
        } catch (IOException e) {
            logger.error("Failed parse current measure", e);
        }

        return null;
    }

    @Override
    public Forecast getForecast() {

        String forecast = restTemplate.getForObject(urlForecast, String.class);

        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.readValue(forecast, Forecast.class);
        } catch (IOException e) {
            logger.error("Failed parse forecast", e);
        }

        return null;
    }
}
