package com.app.webui.service.impl;

import com.app.webui.model.AppUser;
import com.app.webui.service.RestRecipientService;
import com.app.webui.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private @Value("${config.oauth2.usersURI}") String usersUrl;
    private @Value("${config.oauth2.containsUsernameURI}") String containsUsernameUrl;
    private @Value("${config.oauth2.containsEmailURI}") String containsEmailUrl;
    private @Value("${config.oauth2.adminControlURI}") String adminControlUrl;
    private @Value("${config.oauth2.emailURI}") String emailsUrl;

    @Autowired
    @Qualifier("restTemplate")
    private OAuth2RestOperations restTemplate;

    @Autowired
    private RestRecipientService restRecipientService;

    @Override
    public List<AppUser> getUserList() {
        String users = restTemplate.getForObject(usersUrl, String.class);

        ObjectMapper mapper = new ObjectMapper();

        CollectionType javaType = mapper.getTypeFactory()
                .constructCollectionType(List.class, AppUser.class);

        try {
            return mapper.readValue(users, javaType);
        } catch (IOException e) {
            logger.error("Failed parse user list", e);
        }

        return null;
    }

    @Override
    public void addUser(AppUser user) {
        restTemplate.postForObject(usersUrl, user, AppUser.class);
    }

    @Override
    public boolean containsUsername(String username) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(containsUsernameUrl + username, Boolean.class);
    }

    @Override
    public boolean containsEmail(String email) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(containsEmailUrl + email, Boolean.class);
    }

    @Override
    public void deleteUser(AppUser user) {
        user = findByUsername(user.getUsername());
        restRecipientService.deleteRecipient(emailsUrl, user.getEmail());

        String deleteUrl = usersUrl + "/{username}";

        Map<String, String> params = new HashMap<>();
        params.put("username", user.getUsername());

        restTemplate.delete(deleteUrl, params);
    }

    @Override
    public AppUser findByUsername(String username) {
        String url = usersUrl + "/" + username;
        return restTemplate.getForObject(url, AppUser.class);
    }

    @Override
    public void addToAdmins(AppUser user) {
        restTemplate.postForObject(adminControlUrl, user, AppUser.class);
    }

    @Override
    public void removeFromAdmins(AppUser user) {

        String removeAdmin = adminControlUrl + "/{username}";

        Map<String, String> params = new HashMap<>();
        params.put("username", user.getUsername());

        restTemplate.delete(removeAdmin, params);
    }
}
