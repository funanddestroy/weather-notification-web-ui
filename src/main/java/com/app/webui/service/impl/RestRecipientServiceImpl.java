package com.app.webui.service.impl;

import com.app.webui.service.RestRecipientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class RestRecipientServiceImpl implements RestRecipientService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("restTemplate")
    private OAuth2RestOperations restTemplate;

    @Override
    public List<String> getRecipients(String url) {

        String recipients = restTemplate.getForObject(url, String.class);

        ObjectMapper mapper = new ObjectMapper();

        CollectionType javaType = mapper.getTypeFactory()
                .constructCollectionType(List.class, String.class);

        try {
            return mapper.readValue(recipients, javaType);
        } catch (IOException e) {
            logger.error("Failed parse recipient list", e);
        }

        return null;
    }

    @Override
    public void addRecipient(String url, String recipient) {
        restTemplate.postForObject(url, recipient, String.class);
    }

    @Override
    public void deleteRecipient(String url, String recipient) {
        String deleteUrl = url + "/{id}";

        Map<String, String> params = new HashMap<>();
        params.put("id", recipient);

        restTemplate.delete(deleteUrl, params);
    }
}
