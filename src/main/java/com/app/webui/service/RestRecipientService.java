package com.app.webui.service;

import java.util.List;

public interface RestRecipientService {

    List<String> getRecipients(String url);

    void addRecipient(String url, String recipient);

    void deleteRecipient(String url, String recipient);
}
