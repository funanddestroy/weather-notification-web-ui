package com.app.webui.service;

import com.app.webui.model.AppUser;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface UserService {

    void  addUser(AppUser appUser);

    boolean containsUsername(String username);

    boolean containsEmail(String email);

    void deleteUser(AppUser appUser);

    List<AppUser> getUserList();

    AppUser findByUsername(String username);

    void addToAdmins(AppUser user);

    void removeFromAdmins(AppUser user);
}
