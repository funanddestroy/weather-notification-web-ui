package com.app.webui.security;

import com.app.webui.model.AppUser;
import com.app.webui.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class OpenIdConnectFilter extends AbstractAuthenticationProcessingFilter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserService userService;

    private OAuth2RestOperations restTemplate;

    public OpenIdConnectFilter(String defaultFilterProcessesUrl) {
        super(defaultFilterProcessesUrl);
        setAuthenticationManager(new NoopAuthenticationManager());
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {

        OAuth2AccessToken accessToken;
        try {
            accessToken = restTemplate.getAccessToken();
        } catch (final OAuth2Exception e) {
            logger.error("Could not obtain access token", e);
            throw new BadCredentialsException("Could not obtain access token", e);
        }
        try {
            final Jwt tokenDecoded = JwtHelper.decode(accessToken.toString());
            final Map<String, String> authInfo = new ObjectMapper().readValue(tokenDecoded.getClaims(), Map.class);
            final OpenIdConnectUserDetails user = new OpenIdConnectUserDetails(authInfo, accessToken);

            AppUser appUser = userService.findByUsername(authInfo.get("user_name"));
            user.setUserId(appUser.getId().toString());

            List<String> roleNames = appUser.getRoleNames();
            Set<GrantedAuthority> authorities = new HashSet<>();
            if (roleNames != null) {
                for (String role : roleNames) {
                    GrantedAuthority authority = new SimpleGrantedAuthority(role);
                    authorities.add(authority);
                }
            }

            return new UsernamePasswordAuthenticationToken(user, null, authorities);
        } catch (final Exception e) {
            logger.error("Could not obtain user details from token", e);
            throw new BadCredentialsException("Could not obtain user details from token", e);
        }

    }

    public void setRestTemplate(OAuth2RestOperations restTemplate) {
        this.restTemplate = restTemplate;

    }

    private static class NoopAuthenticationManager implements AuthenticationManager {

        private final Logger logger = LoggerFactory.getLogger(this.getClass());

        @Override
        public Authentication authenticate(Authentication authentication) throws AuthenticationException {
            logger.error("No authentication should be done with this AuthenticationManager");
            throw new UnsupportedOperationException("No authentication should be done with this AuthenticationManager");
        }

    }
}

