package com.app.webui.controller.validator;

import com.app.webui.model.AppUser;
import com.app.webui.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Pattern;

@Component
@PropertySource("classpath:validation.properties")
public class UserValidator implements Validator {

    private static final Pattern EMAIL_REGEX =
        Pattern.compile("^[\\w\\d._-]+@[\\w\\d.-]+\\.[\\w\\d]{2,6}$");

    private @Value("${userForm.duplicate.username}") String duplicateUsername;
    private @Value("${userForm.passwordConfirm}") String passwordConfirmWrong;
    private @Value("${userForm.email.invalid}") String invalidEmail;
    private @Value("${userForm.email.duplicate}") String duplicateEmail;

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return AppUser.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        AppUser user = (AppUser) o;

        if (userService.containsUsername(user.getUsername())) {
            errors.reject("123", duplicateUsername);
        }

        if (user.getEmail() != null && !EMAIL_REGEX.matcher(user.getEmail()).matches()) {
            errors.reject("123", invalidEmail);
        }

        if (userService.containsEmail(user.getEmail())) {
            errors.reject("123", duplicateEmail);
        }

        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            errors.reject("123", passwordConfirmWrong);
        }
    }
}
