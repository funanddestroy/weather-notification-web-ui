package com.app.webui.controller;

import com.app.webui.model.AppUser;
import com.app.webui.service.RestRecipientService;
import com.app.webui.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private @Value("${config.oauth2.emailURI}") String emailsUrl;
    private @Value("${config.oauth2.slackURI}") String slackChannelsUrl;

    @Autowired
    private UserService userService;

    @Autowired
    private RestRecipientService restRecipientService;

    @RequestMapping(method = RequestMethod.GET)
    public String adminPage(Model model) {

        List<String> emailList = restRecipientService.getRecipients(emailsUrl);
        List<String> channelsList = restRecipientService.getRecipients(slackChannelsUrl);

        model.addAttribute("emailList", emailList);
        model.addAttribute("channelsList", channelsList);


        return "admin";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String addOrDeleteRecipient(@ModelAttribute("action") String action,
                                       @ModelAttribute("recipient") String recipient,
                                       Model model) {

        switch (action) {
            case "addEmail":
                restRecipientService.addRecipient(emailsUrl, recipient);
                break;
            case "deleteEmail":
                restRecipientService.deleteRecipient(emailsUrl, recipient);
                break;
            case "addChannel":
                restRecipientService.addRecipient(slackChannelsUrl, recipient);
                break;
            case "deleteChannel":
                restRecipientService.deleteRecipient(slackChannelsUrl, recipient);
                break;
        }

        List<String> emailList = restRecipientService.getRecipients(emailsUrl);
        List<String> channelsList = restRecipientService.getRecipients(slackChannelsUrl);

        model.addAttribute("emailList", emailList);
        model.addAttribute("channelsList", channelsList);

        return "admin";
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String users(Model model) {
        model.addAttribute("userList", userService.getUserList());
        return "user_list";
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public String deleteUser(@ModelAttribute("action") String action,
                             @ModelAttribute("username") String username,
                             Model model) {

        switch (action) {
            case "add_admin":
                userService.addToAdmins(new AppUser(0L, username, "", ""));
                break;
            case "remove_admin":
                userService.removeFromAdmins(new AppUser(0L, username, "", ""));
                break;
            case "delete_user":
                userService.deleteUser(new AppUser(0L, username, "", ""));
                break;
        }

        model.addAttribute("userList", userService.getUserList());

        return "user_list";
    }
}
