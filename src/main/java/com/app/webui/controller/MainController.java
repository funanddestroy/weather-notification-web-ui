package com.app.webui.controller;

import java.security.Principal;
import java.util.List;

import com.app.webui.model.AppUser;
import com.app.webui.service.RestForecastService;
import com.app.webui.service.RestRecipientService;
import com.app.webui.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

    private @Value("${config.oauth2.emailURI}") String emailsUrl;
    private @Value("${config.oauth2.slackURI}") String slackChannelsUrl;

    @Autowired
    private UserService userService;

    @Autowired
    private RestRecipientService restRecipientService;

    @Autowired
    private RestForecastService restForecastService;

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model, Principal principal) {
        List<String> emailList = restRecipientService.getRecipients(emailsUrl);

        if (principal != null) {
            String userName = principal.getName();
            AppUser user = userService.findByUsername(userName);

            if (user == null) {
                userService.addUser(new AppUser(0L, userName, "", userName));

                user = userService.findByUsername(userName);
            }

            if (emailList != null) {
                model.addAttribute("containsEmail", emailList.contains(user.getEmail()));
            }
        }
        model.addAttribute("currentMeasure", restForecastService.getCurrentMeasure());
        model.addAttribute("forecast", restForecastService.getForecast());

        return "welcome_page";
    }

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.POST)
    public String addOrDeleteRecipient(@ModelAttribute("action") String action,
                                       @ModelAttribute("recipient") String recipient,
                                       Model model,
                                       Principal principal) {


        String userName = principal.getName();
        AppUser user = userService.findByUsername(userName);

        switch (action) {
            case "subscribe":
                restRecipientService.addRecipient(emailsUrl, user.getEmail());
                break;
            case "unsubscribe":
                restRecipientService.deleteRecipient(emailsUrl, user.getEmail());
                break;
        }

        List<String> emailList = restRecipientService.getRecipients(emailsUrl);

        model.addAttribute("containsEmail", emailList.contains(user.getEmail()));
        model.addAttribute("currentMeasure", restForecastService.getCurrentMeasure());
        model.addAttribute("forecast", restForecastService.getForecast());

        return "welcome_page";
    }







}